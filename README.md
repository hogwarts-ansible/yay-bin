# yay-bin

This role install yay-bin using the aur module.

# Example playbook

```yaml
---
- name: install yay-bin
  hosts: "all"
  roles:
    - role: yay-bin
```

# License

GPLv3

# Author information

Wallun <wallun AT disroot DOT org>
